{if $Action == "" && $Battle == ""}
    {$Battleinfo}
    <br /><br />
    - <a href="battle.php?action=levellist">{$Ashowlevel}</a><br />
    - <a href="battle.php?action=showalive">{$Ashowalive}</a><br />
    - <a href="battle.php?action=monster">{$Ashowmonster}</a><br />
{/if}

{if $Action == "showalive"}
    {$Showinfo} {$Level}...<br /><br />
    <table class="dark">
    <tr>
        <th>{$Lid}</th>
        <th>{$Lname}</th>
        <th>{$Lrank}</th>
        <th>{$Lclan}</th>
        <th>{$Loption}</th>
    </tr>
    {section name=player loop=$Enemyid}
        <tr>
        <td>{$Enemyid[player]}</td>
        <td><a href="view.php?view={$Enemyid[player]}">{$Enemyname[player]}</a></td>
        <td>{$Enemyrank[player]}</td>
        <td>{$Enemytribe[player]}</td>
        <td>- <A href="battle.php?battle={$Enemyid[player]}">{$Aattack}</a></td>
        </tr>
    {/section}
    </table><br />{$Orback} <a href="battle.php">{$Bback}</a>.
{/if}

{if $Action == "levellist"}
    <form method="post" action="battle.php?action=levellist&amp;step=go">
    {$Showall} <input type="text" name="slevel" size="5" /> {$Tolevel} <input type="text" name="elevel" size="5" />
    <input type="submit" value="{$Ago}" /></form>
    {if $Step == "go"}
        <table class="dark">
        <tr>
	<th>{$Lid}</th>
        <th>{$Lname}</th>
        <th>{$Lrank}</th>
        <th>{$Lclan}</th>
        <th>{$Loption}</th>
        </tr>
        {section name=player loop=$Enemyid}
            <tr>
            <td>{$Enemyid[player]}</td>
            <td><a href="view.php?view={$Enemyid[player]}">{$Enemyname[player]}</a></td>
            <td>{$Enemyrank[player]}</td>
            <td>{$Enemytribe[player]}</td>
            <td>- <A href="battle.php?battle={$Enemyid[player]}">{$Aattack}</a></td>
            </tr>
        {/section}
        </table>
    {/if}
{/if}

{if $Battle > "0"}
    <b><u>{$Name} {$Versus} {$Enemyname}</u></b><br />
{/if}

{if $Action == "monster"}
    {if !$Fight && !$Fight1}
        <script src="js/battle.js"></script>
        {$Monsterinfo}
        <br /><br />
        <table class="dark">
        <tr>
        <th>{$Mname}</th>
        <th>{$Mlevel}</th>
        <th>{$Mhealth}</th>
        <th>{$Mturn}</th>
        <th>{$Mfast}</th>
	<th>{$Mamount}<br />{$Mmonsters}</th>
	<th>{$Mamount}<br />{$Mtimes}</th>
	<th>{$Menergy}<br />{$Menergy2}</th>
        </tr>
        {foreach $Monsters as $monster}
            <tr>
            <td>{$monster.name}</td>
            <td>{$monster.level}</td>
            <td>{$monster.hp}</td>
	    <form id="fight{$monster@index}" method="post" action="battle.php?action=monster">
                <td><input type="submit" name="fight1" value="{$Mturn}" />
		    <input type="hidden" value="{$monster.id}" name="mid" />
		    <input type="hidden" name="write" value="Y" />
		</td>
		<td><input type="submit" name="fight" value="{$Mfast}" />
		</td>
		<td>
		    <input id="amount" type="text" size="5" name="razy" value="1" onChange="countEnergy({$monster.level}, {$monster@index});" />
		</td>
		<td>
		    <input id="times" type="text" size="5" name="times" value="1" onChange="countEnergy({$monster.level}, {$monster@index});" />
		</td>
 	    </form>
	    <td id="mon{$monster@index}">{$monster.energy}/{$monster.energy}</td>
            </tr>
        {/foreach}
        </table><br />{$Orback2} <a href="battle.php">{$Bback2}</a>.
    {/if}
{/if}

