<?php
/**
 *   File functions:
 *   Battle Arena - figth between players and player vs monsters
 *
 *   @name                 : battle.php                            
 *   @copyright            : (C) 2004,2005,2006,2011,2012 Vallheru Team based on Gamers-Fusion ver 2.5
 *   @author               : thindil <thindil@vallheru.net>
 *   @version              : 1.6
 *   @since                : 04.10.2012
 *
 */

//
//
//       This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation; either version 2 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program; if not, write to the Free Software
//   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
// $Id$

$title = "Arena Walk";
require_once("includes/head.php");
require_once("includes/funkcje.php");

/**
* Get the localization for game
*/
require_once("languages/".$lang."/battle.php");

global $runda;
global $number;
global $newdate;
global $smarty;
global $db;

if ($player->location == 'Lochy')
  {
    error('Nie możesz zwiedzać areny ponieważ znajdujesz się w lochach.');
  }

if (!isset($_GET['action']) && !isset($_GET['battle']))
{
    $smarty -> assign(array("Battleinfo" => BATTLE_INFO,
                            "Ashowalive" => A_SHOW_ALIVE,
                            "Ashowlevel" => A_SHOW_LEVEL,
                            "Ashowmonster" => A_SHOW_MONSTER));
}

/**
* Start battle
*/
if (isset($_GET['battle'])) 
{
    global $runda;
    global $number;
    global $newdate;
    global $smarty;
    global $db;

    checkvalue($_GET['battle']);
    $arrmenu = array('age','inteli','clas','immunited','strength','agility','attack','miss','magic','speed','cond','race','wisdom','shoot','id','user','level','exp','hp','credits','mana','maps', 'antidote', 'settings', 'newbie', 'oldstats');
    $arrMyequip = $player->equipment();
    $player->curskills(array('weapon', 'shoot', 'dodge', 'cast'), FALSE);
    $arrattacker = $player -> stats($arrmenu);
    $arrattacker['user'] = $arrTags[$player->tribe][0].' '.$player->user.' '.$arrTags[$player->tribe][1];
    $enemy = new Player($_GET['battle']);
    $arrplayer = array('id','user','level','tribe','credits','location','hp','mana','exp','age','inteli','clas','immunited','strength','agility','attack','miss','magic','speed','cond','race','wisdom','shoot','maps','rest','fight', 'antidote', 'settings', 'newbie', 'oldstats');
    $arrEnequip = $enemy -> equipment();
    $enemy->curskills(array('weapon', 'shoot', 'dodge', 'cast'), FALSE);
    $arrdefender = $enemy -> stats($arrplayer);
    $arrdefender['user'] = $arrTags[$arrdefender['tribe']][0].' '.$arrdefender['user'].' '.$arrTags[$arrdefender['tribe']][1];
    $myczar = $db -> Execute("SELECT * FROM czary WHERE gracz=".$player -> id." AND status='E' AND typ='B'");
    $eczar = $db -> Execute("SELECT * FROM czary WHERE gracz=".$arrdefender['id']." AND status='E' AND typ='B'");
    $myczaro = $db -> Execute("SELECT * FROM czary WHERE gracz=".$player -> id." AND status='E' AND typ='O'");
    $eczaro = $db -> Execute("SELECT * FROM czary WHERE gracz=".$arrdefender['id']." AND status='E' AND typ='O'");
    //Count spell damage
    if ($myczar->fields['id'])
      {
	$myczar->fields['dmg'] = $myczar->fields['obr'] * $player->inteli;
      }
    if ($eczar->fields['id'])
      {
	$eczar->fields['dmg'] = $eczar->fields['obr'] * $enemy->inteli;
      }
    if ($myczaro->fields['id'])
      {
	$myczaro->fields['def'] = $myczaro->fields['obr'] * $player->wisdom;
      }
    if ($eczaro->fields['id'])
      {
	$eczaro->fields['def'] = $eczaro->fields['obr'] * $enemy->wisdom;
      }
    $arrElements = array('water' => 'fire',
			 'fire' => 'wind',
			 'wind' => 'earth',
			 'earth' => 'water');
    $arrElements2 = array('W' => 'fire',
			  'F' => 'wind',
			  'A' => 'earth',
			  'E' => 'water');
    $arrElements3 = array('water' => 'W',
			  'fire' => 'F',
			  'wind' => 'A',
			  'earth' => 'E');
    $arrElements4 = array('water' => 'F',
			    'fire' => 'A',
			    'wind' => 'E',
			    'earth' => 'W');
    $arrElements5 = array('W' => 'F',
			  'F' => 'A',
			  'A' => 'E',
			  'E' => 'W');

    for ($i = 2; $i < 6; $i++)
      {
	if ($arrEnequip[$i][10] != 'N')
	  {
	    if ($myczar->fields['id'])
	      {
		if ($arrElements3[$myczar->fields['element']] == $arrEnequip[$i][10])
		  {
		    $arrEnequip[$i][2] = $arrEnequip[$i][2] * 2;
		  }
		elseif ($arrElements4[$myczar->fields['element']] == $arrEnequip[$i][10])
		  {
		    $arrEnequip[$i][2] = ceil($arrEnequip[$i][2] / 2);
		  }
	      }
	    elseif ($arrMyequip[0][10] != 'N')
	      {
		if ($arrMyequip[0][10] == $arrEnequip[$i][10])
		  {
		    $arrEnequip[$i][2] = $arrEnequip[$i][2] * 2;
		  }
		elseif ($arrElements5[$arrMyequip[0][10]] == $arrEnequip[$i][10])
		  {
		    $arrEnequip[$i][2] = ceil($arrEnequip[$i][2] / 2);
		  }
	      }
	    elseif ($arrMyequip[6][10] != 'N')
	      {
		if ($arrMyequip[6][10] == $arrEnequip[$i][10])
		  {
		    $arrEnequip[$i][2] = $arrEnequip[$i][2] * 2;
		  }
		elseif ($arrElements5[$arrMyequip[6][10]] == $arrEnequip[$i][10])
		  {
		    $arrEnequip[$i][2] = ceil($arrEnequip[$i][2] / 2);
		  }
	      }
	  }
	elseif ($arrEnequip[$i][10] == 'N')
	  {
	    if ($myczar->fields['id'])
	      {
		$arrEnequip[$i][2] = 0;
	      }
	    if ($arrMyequip[0][10] != 'N' || $arrMyequip[6][10] != 'N')
	      {
		$arrEnequip[$i][2] = ceil($arrEnequip[$i][2] / 2);
	      }
	  }
	if ($arrMyequip[$i][10] != 'N')
	  {
	    if ($eczar->fields['id'])
	      {
		if ($arrElements3[$eczar->fields['element']] == $arrMyequip[$i][10])
		  {
		    $arrMyequip[$i][2] = $arrMyequip[$i][2] * 2;
		  }
		elseif ($arrElements4[$eczar->fields['element']] == $arrMyequip[$i][10])
		  {
		    $arrMyequip[$i][2] = ceil($arrMyequip[$i][2] / 2);
		  }
	      }
	    elseif ($arrEnequip[0][10] != 'N')
	      {
		if ($arrEnequip[0][10] == $arrMyequip[$i][10])
		  {
		    $arrMyequip[$i][2] = $arrMyequip[$i][2] * 2;
		  }
		elseif ($arrElements5[$arrEnequip[0][10]] == $arrMyequip[$i][10])
		  {
		    $arrMyequip[$i][2] = ceil($arrMyequip[$i][2] / 2);
		  }
	      }
	    elseif ($arrEnequip[6][10] != 'N')
	      {
		if ($arrEnequip[6][10] == $arrMyequip[$i][10])
		  {
		    $arrMyequip[$i][2] = $arrMyequip[$i][2] * 2;
		  }
		elseif ($arrElements5[$arrEnequip[6][10]] == $arrMyequip[$i][10])
		  {
		    $arrMyequip[$i][2] = ceil($arrMyequip[$i][2] / 2);
		  }
	      }
	  }
	elseif ($arrMyequip[$i][10] == 'N')
	  {
	    if ($eczar->fields['id'])
	      {
		$arrMyequip[$i][2] = 0;
	      }
	    if ($arrEnequip[0][10] != 'N' || $arrEnequip[6][10] != 'N')
	      {
		$arrMyequip[$i][2] = ceil($arrMyequip[$i][2] / 2);
	      }
	  }
      }
    if ($eczaro->fields['id'])
      {
	if ($myczar->fields['id'])
	  {
	    if ($myczar->fields['element'] == $eczaro->fields['element'])
	      {
		$eczaro->fields['def'] = ($eczaro->fields['obr'] * $enemy->wisdom) * 2;
	      }
	    elseif ($eczaro->fields['element'] == $arrElements[$myczar->fields['element']])
	      {
		$eczaro->fields['def'] = ($eczaro->fields['obr'] * $enemy->wisdom) / 2;
	      }
	  }
	elseif ($arrMyequip[0][10] != 'N')
	  {
	    if ($arrMyequip[0][10] == $arrElements3[$eczaro->fields['element']])
	      {
		$eczaro->fields['def'] = ($eczaro->fields['obr'] * $enemy->wisdom) * 2;
	      }
	    elseif ($eczaro->fields['element'] == $arrElements2[$arrMyequip[0][10]])
	      {
		$eczaro->fields['def'] = ($eczaro->fields['obr'] * $enemy->wisdom) / 2;
	      }
	  }
	elseif ($arrMyequip[6][10] != 'N')
	  {
	    if ($arrMyequip[6][10] == $arrElements3[$eczaro->fields['element']])
	      {
		$eczaro->fields['def'] = ($eczaro->fields['obr'] * $enemy->wisdom) * 2;
	      }
	    elseif ($eczaro->fields['element'] == $arrElements2[$arrMyequip[6][10]])
	      {
		$eczaro->fields['def'] = ($eczaro->fields['obr'] * $enemy->wisdom) / 2;
	      }
	  }
      }
    if ($myczaro->fields['id'] && $eczar->fields['id'])
      {
	if ($myczar->fields['element'] == $eczaro->fields['element'])
	  {
	    $myczaro->fields['def'] = ($myczaro->fields['obr'] * $player->wisdom) * 2;
	  }
	elseif ($myczaro->fields['element'] == $arrElements[$eczar->fields['element']])
	  {
	    $myczaro->fields['def'] = ($myczaro->fields['obr'] * $player->wisdom) * 2;
	  }
	elseif ($arrEnequip[0][10] != 'N')
	  {
	    if ($arrEnequip[0][10] == $arrElements3[$myczaro->fields['element']])
	      {
		$myczaro->fields['def'] = ($myczaro->fields['obr'] * $player->wisdom) * 2;
	      }
	    elseif ($myczaro->fields['element'] == $arrElements2[$arrEnequip[0][10]])
	      {
		$myczaro->fields['def'] = ($myczaro->fields['obr'] * $player->wisdom) / 2;
	      }
	  }
	elseif ($arrEnequip[6][10] != 'N')
	  {
	    if ($arrEnequip[6][10] == $arrElements3[$myczaro->fields['element']])
	      {
		$myczaro->fields['def'] = ($myczaro->fields['obr'] * $player->wisdom) * 2;
	      }
	    elseif ($myczaro->fields['element'] == $arrElements2[$arrEnequip[6][10]])
	      {
		$myczaro->fields['def'] = ($myczaro->fields['obr'] * $player->wisdom) / 2;
	      }
	  }
      }

    $objFreezed = $db->Execute("SELECT `freeze` FROM `players` WHERE `id`=".$enemy->id);
    if ($objFreezed -> fields['freeze'])
    {
        error(ACCOUNT_FREEZED);
    }
    $objFreezed->Close();
    $gmywt = array(0,0,0,0);
    $gewt = array(0,0,0,0);
    $runda = 0;
    if ($arrMyequip[0][3] != $arrdefender['antidote']) 
    {
        if ($arrMyequip[0][3] == 'D')
        {
            $arrMyequip[0][2] = $arrMyequip[0][2] + $arrMyequip[0][8];
        }
    }
    if ($arrEnequip[0][3] != $player -> antidote) 
    {
        if ($arrEnequip[0][3] == 'D')
        {
            $arrEnequip[0][2] = $arrEnequip[0][2] + $arrEnequip[0][8];
        }
    }
    if (!$arrdefender['id']) 
    {
        error (NO_PLAYER);
    }
    if ($arrdefender['id'] == $player -> id) 
    {
        error (SELF_ATTACK);
    }
    if ($arrdefender['hp'] <= 0) 
    {
        error ($arrdefender['user']." ".IS_DEAD);
    }
    if ($player -> hp <= 0) 
    {
        error (YOU_DEAD);
    }
    if ($player -> energy < 1) 
    {
        error (NO_ENERGY);
    }
    if ($arrdefender['tribe'] == $player -> tribe && $arrdefender['tribe'] > 0) 
    {
        error (YOUR_CLAN);
    }
    if ($arrattacker['newbie'] > 0)
    {
        error (TOO_YOUNG);
    }
    if ($arrdefender['newbie'] > 0)
    {
        error (TOO_YOUNG2);
    }
    if ($arrattacker['clas'] == '') 
    {
        error (NO_CLASS);
    }
    if ($arrdefender['clas'] == '') 
    {
        error (NO_CLASS2);
    }
    if (($arrMyequip[0][0] && $myczar -> fields['id']) || ($arrMyequip[1][0] && $myczar -> fields['id']) || ($arrMyequip[0][0] && $arrMyequip[1][0])) 
    {
        error (SELECT_WEP);
    }
    if (!$arrMyequip[0][0] && !$myczar -> fields['id'] && !$arrMyequip[1][0]) 
    {
        error (NO_WEAPON);
    }
    if ($arrMyequip[1][0] && !$arrMyequip[6][0]) 
    {
        error (NO_ARROWS);
    }
    if (($arrattacker['clas'] == 'Wojownik' || $arrattacker['clas'] == 'Rzemieślnik' || $arrattacker['clas'] == 'Barbarzyńca' || $arrattacker['clas'] == 'Złodziej') && $myczar -> fields['id']) 
    {
        error (BAD_CLASS);
    }
    $span =  ($player -> level - $arrdefender['level']);
    if ($span > 0) 
    {
        error (TOO_LOW);
    }
    if ($arrattacker['immunited'] == 'Y') 
    {
        error (IMMUNITED);
    }
    if ($arrdefender['immunited'] == 'Y') 
    {
        error (IMMUNITED2);
    }
    if ($arrattacker['clas'] == 'Mag' && $player -> mana == 0 && $myczar -> fields['id']) 
    {
        error (NO_MANA);
    }
    if ($player -> location != $arrdefender['location']) 
    {
        error (BAD_LOCATION);
    }
    if ($arrdefender['rest'] == 'Y') 
    {
        error (PLAYER_R);
    }
    if ($arrdefender['fight'] != 0) 
    {
        error (PLAYER_F);
    }
    $smarty -> assign (array("Enemyname" => $arrdefender['user'],
                             "Versus" => VERSUS));
    $db -> Execute("UPDATE `players` SET `energy`=`energy`-1 WHERE `id`=".$player -> id);
    $strMessage = '';
    require_once('includes/battle.php');
    if ($arrattacker['speed'] >= $arrdefender['speed']) 
    {
        attack1($arrattacker, $arrdefender, $arrMyequip, $arrEnequip, $myczar, $eczar, $myczaro, $eczaro,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, $gmywt, $gewt, $arrattacker['id'], $strMessage);
    } 
        else 
    {
        attack1($arrdefender, $arrattacker, $arrEnequip, $arrMyequip, $eczar, $myczar, $eczaro, $myczaro, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, $gmywt, $gewt, $arrattacker['id'], $strMessage);
    }
}
else
  {
    $_GET['battle'] = '';
  }

if (isset($_GET['action']))
  {
    /**
     * Show players on this same level which have a player
     */
    if ($_GET['action'] == 'showalive') 
      {
	$elist = $db -> SelectLimit("SELECT id, user, rank, tribe FROM players WHERE level=".$player -> level." AND hp>0 AND miejsce='".$player -> location."' AND id!=".$player -> id." AND immu='N' AND rasa!='' AND klasa!='' AND rest='N' AND freeze=0", 50);
	$arrid = array();
	$arrname = array();
	$arrrank = array();
	$arrtribe = array();
	while (!$elist -> EOF) 
	  {
	    switch ($elist->fields['rank'])
	      {
	      case 'Admin':
		$arrrank[] = R_ADMIN;
		break;
	      case 'Staff':
		$arrrank[] = R_STAFF;
		break;
	      case 'Member':
		$arrrank[] = R_MEMBER;
		break;
	      default:
		$arrrank[] = $elist -> fields['rank'];
		break;
	      }
	    $arrid[] = $elist -> fields['id'];
	    $arrname[] = $elist -> fields['user'];
	    $arrtribe[] = $elist -> fields['tribe'];
	    $elist -> MoveNext();
	  }
	$elist -> Close();
	$arrTribes = array_unique($arrtribe);
	if (in_array('0', $arrTribes))
	  {
	    unset($arrTribes[array_search('0', $arrTribes)]);
	  }
	if (count($arrTribes))
	  {
	    $objTribes = $db->Execute("SELECT `id`, `name` FROM `tribes` WHERE `id` IN (".implode(',', $arrTribes).")");
	    while (!$objTribes->EOF)
	      {
		foreach ($arrtribe as &$strTribe)
		  {
		    if ($objTribes->fields['id'] == $strTribe)
		      {
			$strTribe = $objTribes->fields['name'];
		      }
		  }
		$objTribes->MoveNext();
	      }
	    $objTribes->Close();
	  }
	foreach ($arrtribe as &$strTribe)
	  {
	    if ($strTribe == '0')
	      {
		$strTribe = 'Brak';
	      }
	  }
	$smarty -> assign ( array("Level" => $player -> level, 
				  "Enemyid" => $arrid, 
				  "Enemyname" => $arrname, 
				  "Enemytribe" => $arrtribe, 
				  "Enemyrank" => $arrrank,
				  "Lid" => L_ID,
				  "Showinfo" => SHOW_INFO,
				  "Lname" => L_NAME,
				  "Lrank" => L_RANK,
				  "Lclan" => L_CLAN,
				  "Loption" => L_OPTION,
				  "Aattack" => A_ATTACK,
				  "Orback" => OR_BACK,
				  "Bback" => B_BACK));
      }

    if ($_GET['action'] == 'levellist') 
      {
	$smarty -> assign(array("Showall" => SHOW_ALL,
				"Tolevel" => TO_LEVEL,
				"Ago" => A_GO));
	if (isset($_GET['step']) && $_GET['step'] == 'go') 
	  {
	    if (!isset($_POST['slevel'])) 
	      {
		error(S_LEVEL);
	      }
	    if (!isset($_POST['elevel'])) 
	      {
		error(E_LEVEL);
	      }
	    checkvalue($_POST['slevel']);
	    checkvalue($_POST['elevel']);
	    $elist = $db -> SelectLimit("SELECT `id`, `user`, `rank`, `tribe` FROM `players` WHERE `level`>=".$_POST['slevel']." AND `level`<=".$_POST['elevel']." AND `hp`>0 AND `miejsce`='".$player->location."' AND `id`!=".$player -> id." AND `immu`='N' AND `rasa`!='' AND `klasa`!='' AND `rest`='N' AND `freeze`=0", 50) or die($db->ErrorMsg());
	    $arrid = array();
	    $arrname = array();
	    $arrrank = array();
	    $arrtribe = array();
	    while (!$elist -> EOF) 
	      {
		switch ($elist->fields['rank'])
		  {
		  case 'Admin':
		    $arrrank[] = R_ADMIN;
		    break;
		  case 'Staff':
		    $arrrank[] = R_STAFF;
		    break;
		  case 'Member':
		    $arrrank[] = R_MEMBER;
		    break;
		  default:
		    $arrrank[] = $elist -> fields['rank'];
		    break;
		  }
		$arrid[] = $elist -> fields['id'];
		$arrname[] = $elist -> fields['user'];
		$arrtribe[] = $elist -> fields['tribe'];
		$elist -> MoveNext();
	      }	    
	    $elist -> Close();
	    $arrTribes = array_unique($arrtribe);
	    if (in_array('0', $arrTribes))
	      {
		unset($arrTribes[array_search('0', $arrTribes)]);
	      }
	    if (count($arrTribes))
	      {
		$objTribes = $db->Execute("SELECT `id`, `name` FROM `tribes` WHERE `id` IN (".implode(',', $arrTribes).")");
		while (!$objTribes->EOF)
		  {
		    foreach ($arrtribe as &$strTribe)
		      {
			if ($objTribes->fields['id'] == $strTribe)
			  {
			    $strTribe = $objTribes->fields['name'];
			  }
		      }
		    $objTribes->MoveNext();
		  }
		$objTribes->Close();
	      }
	    foreach ($arrtribe as &$strTribe)
	      {
		if ($strTribe == '0')
		  {
		    $strTribe = 'Brak';
		  }
	      }
	    $smarty -> assign (array("Enemyid" => $arrid, 
				     "Enemyname" => $arrname, 
				     "Enemytribe" => $arrtribe, 
				     "Enemyrank" => $arrrank,
				     "Lid" => L_ID,
				     "Lname" => L_NAME,
				     "Lrank" => L_RANK,
				     "Lclan" => L_CLAN,
				     "Loption" => L_OPTION,
				     "Aattack" => A_ATTACK));
	  }
      }

    /**
     * Figth with monsters
     */
    if ($_GET['action'] == 'monster') 
      {
	if ($player -> location == 'Lochy')
	  {
	    error(ERROR);
	  }
	if (!isset($_POST['fight']) && !isset($_POST['fight1']) && !isset($_GET['fight1'])) 
	  {
	    $arrMonsters = $db->GetAll("SELECT `id`, `name`, `level`, `hp` FROM `monsters` WHERE `location`='".$player -> location."' ORDER BY `level` ASC");
	    foreach ($arrMonsters as &$arrMonster)
	      {
		$arrMonster['energy'] = floor(1 + ($arrMonster['level'] / 20));
	      }
	    $smarty -> assign (array("Monsters" => $arrMonsters,
				     "Monsterinfo" => MONSTER_INFO,
				     "Mname" => M_NAME,
				     "Mlevel" => M_LEVEL,
				     "Mhealth" => M_HEALTH,
				     "Mfast" => M_FAST,
				     "Mturn" => M_TURN,
				     "Abattle" => A_BATTLE,
				     "Orback2" => OR_BACK2,
				     "Mtimes" => "walk (szybkich)",
				     "Mamount" => "Ilość",
				     "Mmonsters" => "potworów",
				     "Menergy" => "Koszt energii",
				     "Menergy2" => "Szybka/Turowa",
				     "Bback2" => B_BACK2));
	  }
	/**
	 * Turn fight with monsters
	 */
	if (isset($_POST['fight1']) || isset($_GET['fight1'])) 
	  {
	    global $arrehp;
	    global $newdate;
	    if (!isset($_GET['fight1']))
	      {
		checkvalue($_POST['mid']);
		$_GET['fight1'] = $_POST['mid'];
	      }
	    if (!isset($_POST['razy']) && !isset ($_POST['action']))
	      {
		error(ERROR);
	      }
	    if (!intval($_GET['fight1']) || !isset ($_POST['action']) && !intval($_POST['razy']))
	      {
		error (NO_ID);
	      } 
	    if ($player -> hp <= 0) 
	      {
		error (NO_HP);
	      }
	    $enemy1 = $db -> Execute("SELECT * FROM `monsters` WHERE `id`=".$_GET['fight1']);
	    if (!$enemy1 -> fields['id']) 
	      {
		error (NO_MONSTER);
	      }
	    if (!isset($_POST['action']))
	      {
		if ($_POST['razy'] > 20)
		  {
		    error(TOO_MUCH_MONSTERS);
		  }
		$intEnergy = floor(1 + ($enemy1->fields['level'] / 20));
		if ($player->energy < ($_POST['razy'] * $intEnergy))
		  {
		    error ("Nie masz wystarczającej ilości energii.");
		  }
		$_SESSION['razy'] = $_POST['razy'];
		$_SESSION['energy'] = $intEnergy;
	      }
	    require_once("includes/turnfight.php");
	    if ($player -> clas == '') 
	      {
		error (NO_CLASS3);
	      }
	    if ($player->fight > 0 && $player->fight != $_GET['fight1'])
	      {
		error("Już z kimś walczysz!");
	      }
	    $span = ($enemy1 -> fields['level'] / $player -> level);
	    if ($span > 2) 
	      {
		$span = 2;
	      }
	    if (isset ($_POST['write']) && $_POST['write'] == 'Y') 
	      {
		$db -> Execute("UPDATE players SET fight=".$enemy1 -> fields['id']." WHERE id=".$player -> id);
		$_POST['write'] = 'N';
		if (isset($_SESSION['amount']))
		  {
		    for ($k = 0; $k < $_SESSION['amount']; $k ++) 
		      {
			$strIndex = "mon".$k;
			unset($_SESSION[$strIndex]);
		      }
		    unset($_SESSION['exhaust'], $_SESSION['round'], $_SESSION['points'], $_SESSION['amount']);
		  }
	      }
	    /**
	     * Count gained experience
	     */
	    $expgain1 = ceil(rand($enemy1 -> fields['exp1'],$enemy1 -> fields['exp2']) * $span);
	    $expgain = $expgain1;
	    if (!isset($_SESSION['razy']))
	      {
		$_SESSION['razy'] = 1;
	      }
	    if ($_SESSION['razy'] > 1)
	      {
		for ($k = 2; $k <= $_SESSION['razy']; $k++)
		  {
		    $expgain = $expgain + ceil($expgain1 / 5 * (sqrt($k) + 4.5));
		  }
	      }
	    $goldgain = ceil((rand($enemy1 -> fields['credits1'],$enemy1 -> fields['credits2']) * $_SESSION['razy']) * $span);
	    $expgain = $expgain * $_SESSION['energy'];
	    $goldgain = $goldgain * $_SESSION['energy'];
	    $enemy = array("strength" => $enemy1 -> fields['strength'], 
			   "agility" => $enemy1 -> fields['agility'], 
			   "speed" => $enemy1 -> fields['speed'], 
			   "endurance" => $enemy1 -> fields['endurance'], 
			   "hp" => $enemy1 -> fields['hp'], 
			   "name" => $enemy1 -> fields['name'], 
			   "exp1" => $enemy1 -> fields['exp1'], 
			   "exp2" => $enemy1 -> fields['exp2'], 
			   "level" => $enemy1 -> fields['level'],
			   "lootnames" => explode(";", $enemy1->fields['lootnames']),
			   "lootchances" => explode(";", $enemy1->fields['lootchances']),
			   "resistance" => explode(";", $enemy1->fields['resistance']),
			   "dmgtype" => $enemy1->fields['dmgtype']);
	    $arrehp = array ();
	    if (!isset ($_POST['action'])) 
	      {
		unset($_SESSION['miss']);
		$player->energy -= ($_POST['razy'] * $intEnergy);
		if ($player -> energy < 0) 
		  {
		    $player -> energy = 0;
		  }
		$db -> Execute("UPDATE players SET energy=".$player -> energy." WHERE id=".$player -> id);
		turnfight ($expgain,$goldgain,'',"battle.php?action=monster&fight1=".$enemy1 -> fields['id']);
	      } 
	    else 
	      {
		turnfight ($expgain,$goldgain,$_POST['action'],"battle.php?action=monster&fight1=".$enemy1 -> fields['id']);
	      }
	    $enemy1 -> Close();
	  }
	/**
	 * Fast fight with monsters
	 */
	if (isset($_POST['fight'])) 
	  {
	    global $newdate;
	    
	    $_GET['fight'] = $_POST['mid'];
	    checkvalue($_GET['fight']);
	    if (!isset($_POST['razy'])) 
	      {
		$_POST['razy'] = 1;
	      }
	    checkvalue($_POST['razy']);
	    if (!isset($_POST['times']))
	      {
		error(ERROR);
	      }
	    checkvalue($_POST['times']);
	    if ($player->hp <= 0) 
	      {
		error (NO_HP);
	      }
	    if ($_POST['razy'] > 20)
	      {
		error(TOO_MUCH_MONSTERS);
	      }
	    if ($_POST['times'] > 20)
	      {
		error("Zbyt wiele walk na raz!");
	      }
	    $enemy1 = $db -> Execute("SELECT * FROM `monsters` WHERE `id`=".$_GET['fight']);
	    if (!$enemy1 -> fields['id']) 
	      {
		error (NO_MONSTER);
	      }
	    $lostenergy = ($_POST['razy'] * $_POST['times'] * floor(1 + ($enemy1->fields['level'] / 20)));
	    if ($player->energy < $lostenergy) 
	      {
		error ("Nie masz wystarczającej ilości energii.");
	      }
	    if ($player -> clas == '') 
	      {
		error (NO_CLASS3);
	      }
	    $myhp = $player->hp;
	    if ($player->fight > 0 && $player->fight != $_GET['fight'])
	      {
		error("Już z kimś walczysz!");
	      }
	    $enemy1 -> fields['hp'] = ($enemy1 -> fields['hp'] * $_POST['razy']);
	    $enemy = array("strength" => $enemy1 -> fields['strength'], 
			   "agility" => $enemy1 -> fields['agility'], 
			   "speed" => $enemy1 -> fields['speed'], 
			   "endurance" => $enemy1 -> fields['endurance'], 
			   "hp" => $enemy1 -> fields['hp'], 
			   "name" => $enemy1 -> fields['name'], 
			   "exp1" => $enemy1 -> fields['exp1'], 
			   "exp2" => $enemy1 -> fields['exp2'], 
			   "level" => $enemy1 -> fields['level'],
			   "lootnames" => explode(";", $enemy1->fields['lootnames']),
			   "lootchances" => explode(";", $enemy1->fields['lootchances']),
			   "resistance" => explode(";", $enemy1->fields['resistance']),
			   "dmgtype" => $enemy1->fields['dmgtype']);
	    $intAmount = 0;
	    for ($j=1; $j<=$_POST['times']; $j++) 
	      {
		$myexp = $db  -> Execute("SELECT `exp`, `level` FROM `players` WHERE `id`=".$player -> id);
		$player -> exp = $myexp -> fields['exp'];
		$player -> level = $myexp -> fields['level'];
		$span = ($enemy1 -> fields['level'] / $player -> level);
		if ($span > 2) 
		  {
		    $span = 2;
		  }
		/**
		 * Count gained experience
		 */
		$expgain1 = ceil(rand($enemy1 -> fields['exp1'],$enemy1 -> fields['exp2']) * $span);
		$expgain = $expgain1;
		if ($_POST['razy'] > 1)
		  {
		    for ($k = 2; $k <= $_POST['razy']; $k++)
		      {
			$expgain = $expgain + ceil($expgain1 / 5 * (sqrt($k) + 4.5));
		      }
		  }
		$goldgain = ceil((rand($enemy1 -> fields['credits1'],$enemy1 -> fields['credits2']) * $_POST['razy']) * $span);
		$expgain = $expgain * floor(1 + ($enemy1->fields['level'] / 20));
		$goldgain = $goldgain * floor(1 + ($enemy1->fields['level'] / 20));
		if ($player->antidote == 'R')
		  {
		    $blnRessurect = TRUE;
		  }
		else
		  {
		    $blnRessurect = FALSE;
		  }
		fightmonster ($enemy, $expgain, $goldgain, $_POST['times']);
		$intAmount++;
		if (($player -> hp <= 0) || ($player->antidote == '' && $blnRessurect))
		  {
		    break;
		  }
	      }
	    $enemy1->Close();
	  }
      }
  }
else
  {
    $_GET['action'] = '';
  }

/**
* Initialization of variables
*/
if (!isset($_GET['step'])) 
{
    $_GET['step'] = '';
}

if (!isset($_GET['fight'])) 
{
    $_GET['fight'] = '';
}

if (!isset($_GET['fight1'])) 
{
    $_GET['fight1'] = '';
}

if (!isset($_GET['dalej'])) 
{
    $_GET['dalej'] = '';
}

if (!isset($_GET['next'])) 
{
    $_GET['next'] = '';
}

/**
* Assign variables and display page
*/
$smarty -> assign (array("Action" => $_GET['action'], 
                         "Battle" => $_GET['battle'], 
                         "Step" => $_GET['step'], 
                         "Fight" => $_GET['fight'], 
                         "Fight1" => $_GET['fight1'], 
                         "Dalej" => $_GET['dalej'], 
                         "Next" => $_GET['next']));
$smarty -> display ('battle.tpl');

require_once("includes/foot.php");
?>
