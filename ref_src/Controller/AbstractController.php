<?php

namespace NeewHeru\Controller;

use NeewHeru\ContinuityResponse;
use Symfony\Component\HttpFoundation\Response;

class AbstractController {
    protected function returnResponse($content = '', $status = 200, $headers = [])
    {
        return new Response($content, $status, $headers);
    }

    protected function returnContinuityResponse()
    {
        return new ContinuityResponse();
    }
}