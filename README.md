## Requirements
* docker-compose
* some locally installed mysql database viewer
## Installation
### Notes
#### Dockerized MySQL database credentials
* database host: `database`
* database name: whatever you made in point 2 of the Procedure
* database user: `root`
* database password: `root`
### Procedure
1. run `docker-compose up -d`
2. enter the container by running `docker-compose exec web bash`
3. go to parent directory with `cd ..` and run `composer install`
4. exit the container by `exit`
5. create database with locally installed mysql database viewer
6. goto whatever your docker machine ip address is `/install/install.php`
7. fill the first form taking as fit
8. on second stage select `mail`
9. voila - goto your docker machine ip address and you should see `index.php`
## Running and Stopping
* to run use `docker-compose up -d`
  * its a good idea to run `composer install` inside the container (as described in points 2-4 of Installation Procedure) after pulling any changes
* to stop use `docker-compose down`